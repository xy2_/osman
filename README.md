# Miscellanous overlay
To add this overlay:

## With emaint (does not require additional software)

Add these lines to /etc/portage/repos.conf/osman.conf:

```
location = /usr/local/portage/osman
sync-type = git
sync-uri = https://github.com/xy2_/osman.git
priority = 50
auto-sync = yes
```

then `emaint sync -r osman`.

## With layman 
`layman -o https://gitlab.com/xy2_/osman/raw/master/osman-overlay.xml -f -a osman`