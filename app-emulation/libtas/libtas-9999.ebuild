# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit cmake-utils

DESCRIPTION="GNU/Linux software to (hopefully) give TAS tools to games"
HOMEPAGE="https://github.com/clementgallet/libTAS"
EGIT_REPO_URI=( {https,git}://github.com/clementgallet/libTAS )

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="
	x11-libs/libX11
	x11-libs/fltk
	dev-libs/libtar
	sys-libs/zlib
	media-libs/libsdl2
	media-video/ffmpeg
	media-libs/freetype
	media-libs/fontconfig
"
RDEPEND="${DEPEND}"
